#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <dirent.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <zip.h>
#include <libexplain/fileno.h>

// clear;gcc -Wall poc.c -o poc.x -lzip -lexplain && ./poc.x
// you need libzip-dev and libexplain-dev on your system

// the goal is
// 1.to join the chunks in ram
// 2. extract a .blend of the zip (main_file) without any file system access
// 3. give to blender thought the sheepit_read_data struct (only the struc creation is part of the PoC)

struct sheepit_read_data {
  void *contents;
  int size;
};

struct sheepit_read_data *sheepit_read_chunks(char **chunks, int chunks_size, const char *password, const char *main_file);



static int get_data(void **datap, size_t *sizep, char **chunks, int chunks_size) {
    for (int i = 0; i < chunks_size; i++) {
        printf("String %d %s\n", i, chunks[i]);
    }

    unsigned long total_size = 0;
    for (int i = 0; i < chunks_size; i++) {
        struct stat info;
        if (stat(chunks[i], &info) != 0) {
            // error handling
        }
        printf("FILE SIZE: %lu\n", (unsigned long) info.st_size);
        total_size += (unsigned long) info.st_size;
    }
    printf("total SIZE: %lu\n", (unsigned long) total_size);

    char *content = (char *) malloc(total_size);

    size_t total_bytes_read = 0;
    size_t buffer_size = total_size;	// 1024 * 1024; // 1 MB, for example
    for (int i = 0; i < chunks_size; i++) {
        struct stat info;
        if (stat(chunks[i], &info) != 0) {
            // error handling
        }
        FILE *infile = fopen(chunks[i], "rb");
        if (infile == NULL) {
            // error handling
        }

        size_t bytes_read = fread(content + total_bytes_read, 1, buffer_size - total_bytes_read, infile);
        printf("bytes_read %d\n", bytes_read);
        if (bytes_read < 0) {
            perror("Error reading from file");
            return -8;
        }

        total_bytes_read += bytes_read;
        fclose(infile);
    }

    *sizep = (size_t)total_size;
    *datap = content;


//     /* example implementation that reads data from file */
//     struct stat st;
//     FILE *fp;
//
//     if (stat(archive, &st) < 0) {
// 	if (errno != ENOENT) {
// 	    printf("can't stat %s: %s\n", archive, strerror(errno));
// 	    return -1;
// 	}
//
// 	*datap = NULL;
// 	*sizep = 0;
//
// 	return 0;
//     }
//
//     if ((*datap = malloc((size_t)st.st_size)) == NULL) {
// 	printf("can't allocate buffer\n");
// 	return -1;
//     }
//
//     if ((fp=fopen(archive, "r")) == NULL) {
// 	free(*datap);
// 	printf("can't open %s: %s\n", archive, strerror(errno));
// 	return -1;
//     }
//
//     if (fread(*datap, 1, (size_t)st.st_size, fp) < (size_t)st.st_size) {
// 	free(*datap);
// 	printf("can't read %s: %s\n", archive, strerror(errno));
// 	fclose(fp);
// 	return -1;
//     }
//
//     fclose(fp);
//
//     *sizep = (size_t)st.st_size;
    return 0;
}



struct sheepit_read_data * sheepit_read_chunks(char **chunks, int chunks_size, const char *password, const char *main_file) {
    printf("****************************************\n");
    printf("sheepit_read_chunks debug 00\n");


    const char *archive;
    zip_source_t *src;
    zip_t *za;
    zip_error_t error;
    void *data;
    size_t size;

    //     if (argc < 2) {
    // 	printf("usage: %s archive\n", argv[0]);
    // 	return 1;
    //     }
    archive = chunks[0];

    /* get buffer with zip archive inside */
    if (get_data(&data, &size, chunks, chunks_size) < 0) {
         printf("failed 01\n");
         return NULL;
    }

    zip_error_init(&error);
    /* create source from buffer */
    if ((src = zip_source_buffer_create(data, size, 1, &error)) == NULL) {
        printf("can't create source: %s\n", zip_error_strerror(&error));
        free(data);
        zip_error_fini(&error);
        return NULL;
    }

    /* open zip archive from source */
    if ((za = zip_open_from_source(src, 0, &error)) == NULL) {
       printf("can't open zip from source: %s\n", zip_error_strerror(&error));
        zip_source_free(src);
        zip_error_fini(&error);
        return NULL;
    }
    zip_error_fini(&error);
    printf("sheepit_read_chunks debug 10\n");

    /* we'll want to read the data back after zip_close */
    zip_source_keep(src);

    /* modify archive */
    if (zip_set_default_password(za, password) == -1) {
        printf("Failed to set password for ZIP archive.\n");
        return NULL;
    }

//     zip_uint64_t index = zip_name_locate(za, main_file, ZIP_FL_NOCASE);
//     if (index == -1) {
//         printf("file not found %s\n", main_file);
//         return NULL;
//     }

    struct zip_stat file_info;
    zip_stat_init(&file_info);
    zip_uint64_t index = zip_stat(za, main_file, ZIP_FL_NOCASE  ,&file_info);
    if (index == -1) {
        printf("file not found %s\n", main_file);
        return NULL;
    }

     printf("sheepit_read_chunks debug 20 index: %d\n", index);

    // Step 4: Extract the data from the found file.
    struct zip_file *file = zip_fopen_index(za, index, 0);
    if (!file) {
        printf("Failed to open the file in the archive.\n");
        return NULL;
    }

    char *file_data = malloc(file_info.size);
    if (!file_data) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    printf("sheepit_read_chunks debug 30\n");

    zip_fread(file, file_data, file_info.size);
    zip_fclose(file);

//     struct zip_stat sb;
//     for (int i = 0; i < zip_get_num_entries(za, 0); i++) {
//         if (zip_stat_index(za, i, 0, &sb) == 0) {
//             int len;
//             printf("sheepit_read_chunks 08 **************************\n");
//             len = strlen(sb.name);
//             printf("Name: [%s], ", sb.name);
//             printf("Size: [%llu], ", sb.size);
//             printf("mtime: [%u]\n", (unsigned int) sb.mtime);
//
//             if (strcmp(sb.name, main_file) == 0) {
//                 // get the data :)
//
//             }
//         }
//     }

    //return NULL;


    if (zip_close(za) < 0) {
        printf("can't close zip archive '%s': %s\n", archive, zip_strerror(za));
        return NULL;
    }

    printf("sheepit_read_chunks debug 50\n");

    printf("sheepit_read_chunks 99 END\n");
    struct sheepit_read_data *ret = (struct sheepit_read_data *) malloc(sizeof(struct sheepit_read_data));
    ret->contents = file_data;
    ret->size = file_info.size;

    printf("----------------------------\n");
    return ret;
}

int main() {
    char *chunks[4] = {
        "chunks/split_4/cb18b775d0f675cee0aefee635ba0254",
        "chunks/split_4/7f1f0d292f5366a38481fb03a18b0655",
        "chunks/split_4/bf97bf84facfcb3c5204c70ea311f43d",
        "chunks/split_4/5dd9f085e39a1b57182fba3f1df50847"
    };
    struct sheepit_read_data *contents_from_zip = sheepit_read_chunks(chunks, 4, "L74aPNmrNtjhWfy8ix1vCWgEC90n5Qfw", "worldmap3.0.blend");

//     char *chunks[1] = {
//         "chunks/split_1/21b188f39f7b62fba9ca4bf28cd155f3"
//     };
//     struct sheepit_read_data *contents_from_zip = sheepit_read_chunks(chunks, 1, "0", "worldmap3.0.blend");

  return 0;
}
